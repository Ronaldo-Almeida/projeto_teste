#!/bin/bash
source ~/.bashrc
cd .
 
bundle install
 
RAILS_ENV=test bundle exec rake db:drop db:create db:migrate db:schema:load
bundle exec rake test
